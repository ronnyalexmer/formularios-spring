package edu.uoc.rmedinaeras.controllers;

import edu.uoc.rmedinaeras.beans.FormuPersonas;
import edu.uoc.rmedinaeras.enums.Genero;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

import static edu.uoc.rmedinaeras.Utils.SessionUtils.REGISTRO_FORMU;

@Controller
public class SignUpPersonController {
    static Log Log = LogFactory.getLog(SignUpPersonController.class.getSimpleName());

    @RequestMapping(value = {"/signup/index.html", "/signup", "/registro/index.html", "/registro"}, method = RequestMethod.GET)
    public ModelAndView getSignUpView() {
        Log.info("getSignUpView INI");
        ModelAndView mv = new ModelAndView("signUp");
        mv.addObject("welcome", "Welcome to this amazing site");
        mv.addObject("generos", Genero.values());
        Log.info("getSignUpView END");
        return mv;
    }

    @RequestMapping(value = {"/signup/index.html", "/signup", "/registro/index.html", "/registro"}, method = RequestMethod.POST)
    public ModelAndView postSignUpView(@ModelAttribute("FormuPersonas") FormuPersonas persona, HttpServletRequest request) {
        Log.info("reditect To Welcome view");
        persona.setRegistro(new Date());
        request.getSession().setAttribute(REGISTRO_FORMU, persona);
        return new ModelAndView("redirect:/welcome");
    }

    /**
     * Instancia por defecto para el
     * modelAttribute de la jsp
     *
     * @return new FormuPersonas()
     */
    @ModelAttribute("FormuPersonas")
    public FormuPersonas defaultInstance() {
        return new FormuPersonas();
    }
}
