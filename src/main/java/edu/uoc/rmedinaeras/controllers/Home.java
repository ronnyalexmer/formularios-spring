package edu.uoc.rmedinaeras.controllers;

import edu.uoc.rmedinaeras.beans.FormuPersonas;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

import static edu.uoc.rmedinaeras.Utils.SessionUtils.REGISTRO_FORMU;

@Controller
public class Home {
    static org.apache.commons.logging.Log Log = LogFactory.getLog(Home.class.getSimpleName());

    @RequestMapping(value = "/welcome", method = RequestMethod.GET)
    public ModelAndView getHome(HttpServletRequest request) {
        ModelAndView mv = new ModelAndView("welcome");
        FormuPersonas person = (FormuPersonas) request.getSession().getAttribute(REGISTRO_FORMU);
        mv.addObject("welcome", "Welcome to me webSite");
        if (person != null && person.getUsername() != null) {
            mv.addObject("persona", person);
        }
        return mv;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home() {
        return "home";
    }

}
