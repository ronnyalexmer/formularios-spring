package edu.uoc.rmedinaeras.enums;

public enum Genero {
    HOMBRE("Hombre", "H"), MUJER("Mujer", "M"), TRANS("Transgenero", "T"), FEMALE("Hermafrodita", "HM");

    private final String name;
    private final String value;

    private Genero(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }
}
