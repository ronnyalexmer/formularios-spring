package edu.uoc.rmedinaeras.dao;

import edu.uoc.rmedinaeras.beans.FormuPersonas;

import java.util.List;

public interface FormuPersonasDAO {
    public boolean registrar(FormuPersonas personas);

    public List<FormuPersonas> listar();
}
