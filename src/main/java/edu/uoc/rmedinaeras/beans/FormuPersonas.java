package edu.uoc.rmedinaeras.beans;

import edu.uoc.rmedinaeras.Utils.FormuUtils;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "usuarios")
public class FormuPersonas extends Person implements Serializable {
    @Id
    @Column(name = "dni")
    private String dni;
    private String username;

    @Temporal(TemporalType.DATE)
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private String registro;

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRegistro() {
        return registro;
    }

    public void setRegistro(Date registro) {
        this.registro = FormuUtils.ddMMyyyyhhmmss(registro);
    }

    @Override
    public String toString() {
        return super.toString() + "\nFormuPersonas{" +
                "dni='" + dni + '\'' +
                ", username='" + username + '\'' +
                ", registro='" + registro + '\'' +
                '}';
    }
}
