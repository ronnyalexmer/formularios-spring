package edu.uoc.rmedinaeras.Utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class FormuUtils {
    public static String ddMMyyyyhhmmss(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
        return sdf.format(date);
    }
}
