<!DOCTYPE html>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
<head>
    <title>Registro</title>
</head>
<body>
<h1>${welcome}</h1>

<%--@elvariable id="FormuPersonas" type="edu.uoc.rmedinaeras.beans.FormuPersonas"--%>
<form:form method="POST" action="/signup" name="registro" modelAttribute="FormuPersonas">
    <table>
        <tbody>
        <tr>
            <td><form:label path="nombre">Nombre</form:label></td>
            <td><form:input path="nombre"/></td>
        </tr>
        <tr>
            <td><form:label path="apellidos">Apellidos</form:label></td>
            <td><form:input path="apellidos"/></td>
        </tr>
        <tr>
            <td><form:label path="nacimiento">Fecha de nacimiento dd/MM/yyyy</form:label></td>
            <td><input type="date" name="nacimiento"></td>
        </tr>
        <tr>
            <td><form:label path="nacimiento">Genero</form:label></td>
            <td><form:select path="genero"> <form:options items="${generos}" itemValue="value" itemLabel="name"/>
            </form:select>
            </td>
        </tr>
        <tr>
            <td><form:label path="dni">DNI</form:label></td>
            <td><form:input path="dni" maxlength="10"/></td>
        </tr>
        <tr>
            <td><form:label path="username">UserName</form:label></td>
            <td><form:input path="username" maxlength="10"/></td>
        </tr>
        </tbody>
        <tfoot>
        <form:button>Registrar</form:button>
        </tfoot>
    </table>
</form:form>
</body>
</html>
